import praw
from openpyxl import Workbook

#Ask for username and password to login

user = raw_input('Please enter you username: ')
pswd = raw_input('Please enter your password: ')

#Limits the amount of submissions the request returns

thing_limit = None

#Instantiate a Reddit class and login with it. Connect using API
r = praw.Reddit('reddit_test by /u/pricolascage')
r.login(user, pswd, disable_warning=True)

#Create an instance of an item container with the specified limit
#of saved submission grabs.
saved_submissions = r.user.get_saved(limit = thing_limit)

#Create constant colors for the workbook
#reddit_blue = Font(color='CEE3F8')

#Create a workbook to store data and an active sheet.
wb = Workbook()
ws = wb.active
ws['B1'] = 'Submission Title'
ws['C1'] = 'Subreddit'
ws['D1'] = "Author"
ws['E1'] = "Score"
#submission_attributes = ws['B1':'E1']
#submission_attributes.font = reddit_blue

#While loop is required for the try/except loop.
#For loop goes through the submissions container and prints
#out individual attributes of the submission. Currently testing
#what this loop will be able to do with submssion attributes.
#for submissions in saved_submissions:

sentinel = 0

while sentinel != 1:

	try:

		for row, submission in enumerate(saved_submissions):

			column_cell = 'B'
			ws[column_cell+str(row+2)] = submission.title.encode('utf-8')

			column_cell = 'C'
			ws[column_cell+str(row+2)] = str(submission.subreddit)

			column_cell = 'D'
			ws[column_cell+str(row+2)] = str(submission.author)
 
			column_cell = 'E'
			ws[column_cell+str(row+2)] = str(submission.score)

	except AttributeError:

		continue

	sentinel += 1

#Test area to make sure limited number of attributes print
#try:
#	print submissions.title
#	print submissions.subreddit
#	print submissions.author
#	print "\n"

#except AttributeError:

#	pass

wb.save('reddit_test.xlsx')